
import { time } from 'console';
import { Channel } from 'diagnostics_channel';
import {Client, TextChannel, VoiceChannel, MessageEmbed, MessageButton, MessageActionRow, UserResolvable, RoleResolvable, PermissionOverwriteOptions, GuildChannel, GuildMember, Message} from 'discord.js'
//requirements for exporter
require('dotenv').config();
const { exec } = require("child_process");
//import { exec } from "child_process";

let expChannel:TextChannel;

export class Clean {

    private targetTime:number;
    private client:Client;

    

    //target time set with DAILY TIME CYCLING (based on 24-hour formatting:1-24)
    constructor(client:Client) {
        this.targetTime = 10;
        if (process.env.TARGET_TIME!==undefined) {parseInt(process.env.TARGET_TIME,10);}
        this.client = client;
        this.scheduleCleanTask();
    }

    private runCleanTask() {

        // Note: 60000 = 1 minute, 604800000 = one week
        // Warning Lim = Initial warning, Time limit = time after warning till closure
        let WARNING_LIMIT = 604800000 * 2; // two weeks
        let TIME_LIMIT = 259200000; // 3 days
        if (process.env.WARNING_LIMIT!==undefined){
            WARNING_LIMIT = parseInt(process.env.WARNING_LIMIT,10);
        }
        if (process.env.TIME_LIMIT!==undefined){
            TIME_LIMIT = parseInt(process.env.TIME_LIMIT,10);
        }

        console.log("Clean Task Begun");


        
        const currentTime = Date.now();
        if(!this.client || !this.client.channels) return;

        const guild = this.client.guilds.fetch(process.env.GUILD_ID as string);
        guild.then(g => { g.channels.cache.forEach(async (channel) => {
            if (!channel || 
                 channel.type !== "GUILD_TEXT" || 
                !channel.name.startsWith("breakout")) {
                    //console.log("TEST: breakout detected, name: " + channel.name);
                    return;
                }
            
            let timeStamp = channel.lastMessage?.createdTimestamp

            channel.messages.fetch({limit:1}).then(messages => {
                let lastMessage = messages.first();
                timeStamp = channel.lastMessage?.createdTimestamp
                //console.log("ts: " + timeStamp);
            })

            //const timeStamp = channel.lastMessage?.createdTimestamp

            //grab companion Voice Channel for closure
            const companionChannel = channel.guild.channels.cache.find(
                (ch) => ch.name === channel.name && ch.type === "GUILD_VOICE"
            ) as VoiceChannel;

            
            if(!timeStamp) return;


            const timeDifference = currentTime-timeStamp;

            //WARNING_LIMIT is only handled at this point, when closure is imminent
            //a timeout will be set for closure
            if (timeDifference >= WARNING_LIMIT) {
                //begin closure count down
                const setTime = await this.enableClosure(channel, companionChannel, TIME_LIMIT);
                //show button choices for user
                await this.showButtons(channel, companionChannel, setTime).catch(console.error);
            }        


            // Grab text channel used for exporting
            expChannel = channel.guild.channels.cache.find(
                (ch) => ch.name === 'auto-export-log') as TextChannel;


        })});
    }

    private scheduleCleanTask():void {
        console.log("automated breakout clean scheduled...");
        //Solve for the time in which task will be executed
        const currentTime = new Date();
        const target = new Date(
            currentTime.getFullYear(),
            currentTime.getMonth(),
            currentTime.getDate(),
            this.targetTime,
            0,
            0);
        //Time until task calculation
        let remainingTime = target.getTime() - currentTime.getTime();
        //Check to see if time has been met, reset remainingTime for the next day (+24hrs)
        if (remainingTime <= 0) { remainingTime += 24*60*60*1000; }
        //setInterval(this.runCleanTask.bind(this), remainingTime); //THIS IS THE CODE FOR RUNNING ON 24 HOUR CYCLE



        // This line determines frequency at which clean task runs, 60000 = once per minute
        setInterval(this.runCleanTask.bind(this), 60000*5);
    }

    //catalyst function to initiate cleaning
    public startCleaning():void {
       console.log("automated breakout clean commenced...");
       this.scheduleCleanTask();
    }

    //button appearance + functionality
    async showButtons(channel: TextChannel, companionChannel: VoiceChannel, setTime:number|undefined) {
        //create two buttons, one for it to stay open one to close
        const channel_ID = channel.id;
        const time = Date.now();
        const closure_ID = 'leave-' +  channel_ID + '-' + time;
        const open_ID = 'open-' +  channel_ID + '-' + time;
        const user_Question = 'Due to inactivity, this channel will be closed soon. Please ' +
                              'utilize the interactive button or leave a message in the chat ' +
                              'to allow the space to remain open or leave. You have three days.'

        const closureButton = new MessageButton()
        .setStyle('PRIMARY')
        .setLabel('LEAVE')
        .setCustomId(closure_ID);

        const openButton = new MessageButton()
        .setStyle('SUCCESS')
        .setLabel('STAY')
        .setCustomId(open_ID);
        //create row with buttons
        const row = new MessageActionRow().addComponents(openButton,closureButton);

        const button = new MessageEmbed()
        .setTitle('Warning Expiring Text Channel')
        .setDescription(user_Question)
        .setColor('#800000');

        //send the button system to the user
        const button_Message = await channel.send({ embeds: [button] }).catch(console.error);
        const row_Buttons = await channel.send({ components: [row] }).catch(console.error);

        //Button interaction response handling
        this.client.on('interactionCreate', async (interaction) => {
            console.log(`Interaction received: ${interaction.type}`);
            
            if (!interaction.isButton()) return;

            const comp = interaction.customId.split('-');
            if (comp[0] === 'leave') {
                for (let i = 0; i < comp.length; i++) {
                    console.log(comp[i]);
                }

                if (comp[1] === channel.id) {
                    await interaction.deferUpdate().catch(console.error);
                    const originalMessage = await interaction.channel?.messages.fetch(interaction.message.id).catch(console.error);
                    if (originalMessage) {
                        const embed = originalMessage.embeds[0];
                        const newEmbed = new MessageEmbed(embed);
                        newEmbed.setDescription('Thank you! You have chosen to leave.');
                        await originalMessage.edit({ embeds: [newEmbed], components: [] }).catch(console.error);
                    }

                    await this.leaveBreakout(interaction, channel, companionChannel).catch(console.error);
                }

            } else if (comp[0] === 'open') {
                await interaction.deferUpdate().catch(console.error);
                const originalMessage = await interaction.channel?.messages.fetch(interaction.message.id).catch(console.error);;
                if (originalMessage) {
                    await this.disableClosure(setTime); //end the closure timer
                    const embed = originalMessage.embeds[0];
                    const newEmbed = new MessageEmbed(embed);
                    newEmbed.setDescription('Thank you! You have chosen to stay.');

                    await originalMessage.edit({ embeds: [newEmbed], components: [] }).catch(console.error);
                }
            }
        });
    }

    //leave breakout functionality
    async leaveBreakout(interaction:any, channel: TextChannel, companion: VoiceChannel | undefined) {
        const textChannel: TextChannel = channel;
        // cancels leave if not in breakout.
        if (!textChannel.name.startsWith("breakout")) {
            return;
        }

        const member = interaction.member as GuildMember;

        // update permissions to remove leaving member.
        await this.updateOverwrite(textChannel, member, {
            VIEW_CHANNEL: false,
            SEND_MESSAGES: false
        }).catch(console.error);

        if (companion){
            await this.updateOverwrite(companion, member, {
                VIEW_CHANNEL: false
            }).catch(console.error);
        }

        await interaction
            .reply(`@${member.displayName} has left the breakout.`).catch(console.error);

        let bitfieldTotal = 0n;

        const members = textChannel.permissionOverwrites.cache
            .filter((po) => po.type === "member" && !po.allow.bitfield)
            .map((m) => m.id);

        console.log(`exporting permissions for '${textChannel.name}'`);

        //const fetchedChannel = await textChannel.fetch(true);
        const permissionOverwrites = textChannel.permissionOverwrites.cache.map(
            (po) => po
        );

        const guild = textChannel.guild;

        // remove role if all users have left.
        for (const permissionOverwrite of permissionOverwrites) {
            console.log(
                `${permissionOverwrite.type}, ${permissionOverwrite.id}, ALLOW, ${permissionOverwrite.allow.bitfield}`
            );
            console.log(
                `${permissionOverwrite.type}, ${permissionOverwrite.id}, DENY,  ${permissionOverwrite.deny.bitfield}`
            );

            if (permissionOverwrite.type === "role") {
                const role = guild.roles.cache.find(
                    (r) => r.id === permissionOverwrite.id
                );

                // if all members presently in the role have left the channel, remove the role.
                if (role && role.members.every((m) => members.includes(m.id))) {
                    await permissionOverwrite
                        .edit({ VIEW_CHANNEL: false, SEND_MESSAGES: false })
                        .catch(console.error);
                    continue;
                }
            }

            // if the overwrite is a user a role that still contains present users, add to bitfield total.
            bitfieldTotal += permissionOverwrite.allow.bitfield;
        }

        console.log(`bitfieldTotal: ${bitfieldTotal}`);

        // remove breakout if no one has permission
        if (!bitfieldTotal) {
            await this.exp(textChannel, companion).catch(console.error);
        }

        return;
    }


    // helper function for updating permission overwrites.
    async updateOverwrite(
        channel: GuildChannel,
        roleOrMember: UserResolvable | RoleResolvable,
        permissions: PermissionOverwriteOptions
    ) {
        await channel.permissionOverwrites
            .edit(roleOrMember, permissions)
            .then(() =>
                console.log(`[ops] overwrote permission for '${channel.name}'`)
            )
            .catch(console.error);
    }

    // helper function for deleting a breakout.
    async deleteBreakout(
        textChannel: TextChannel,
        voiceChannel?: VoiceChannel
    ) {
        //follow with deletion
        console.log('---deletion in progress---');
        await Promise.all([textChannel.delete(), voiceChannel?.delete()])
            .then((res) =>
                console.log(
                    `[ops] deleted breakout '${(res[0] as TextChannel).name}'`
                )
            )
            .catch(console.error);
    }
    //helper function for exporting breakout information; run prior to deletion
    async exp(tc : TextChannel, vc : undefined|VoiceChannel) {
        /*const SEM_PERIOD = process.env.EXPORT_FOLDER as string;
        const ACC_TOKEN = process.env.DISCORD_APPLICATION_TOKEN as string;*/

        // Get current time
        const now = new Date();
        const month = now.getMonth() + 1;
        const year = now.getFullYear();
        const dir = month + '.' + year as string;

        const valueClass = tc.id.toString();
        console.log('---export action in progress---');

        expChannel.send('EXPORT: adding channel ' + tc.name + ' to export list');
        expChannel.send('++add_channel ' + valueClass);

        setTimeout(() => {
            expChannel.send('EXPORT: beginning daily export to directory: ' + dir);
            expChannel.send('++export_log ' + dir);
        
            // Delay for 5 minutes (300,000 milliseconds) before executing the third command
            setTimeout(() => {
                this.deleteBreakout(tc, vc).catch(console.error);
                expChannel.send('++cat_export clear');
                expChannel.send('EXPORT: Deleted channel ' + tc.name + ' and removed from list');
            }, 300000); // 300,000 milliseconds = 5 minutes
        
        }, 5000); // 5000 milliseconds = 5 seconds between add to list and export
       

        /*
        exec(`dotnet ${process.env.EXPORTER_LOC}/DiscordChatExporter.Cli.dll export -t ${ACC_TOKEN} -c ${valueClass} -o ${process.env.EXPORTS_LOC}/${SEM_PERIOD}/%T/HTML/%C.html`,
            (error:Error|null, stdout:string, stderr:string) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            }
            console.log(`stdout: ${stdout}`);
        }).on('close', () => {
            this.deleteBreakout(tc, vc).catch(console.error);
        });
        */
    }

    //function to begin closure timer
    async enableClosure(channel:TextChannel, companionChannel:undefined|VoiceChannel,
        TIME_LIMIT:number):Promise<number> {
        const resultant:NodeJS.Timeout = setTimeout((async() => {
            console.log("time limit surpassed, closure in progress...");
            await this.exp(channel,companionChannel).catch(console.error);
        }), TIME_LIMIT);
        
        return resultant as unknown as number;
    }

    //function to end closure timeout for associated TC and VC when interaction occurs
    async disableClosure(timeID:number|undefined) {
        if (timeID) {
            clearTimeout(timeID);
        }
    }
}

