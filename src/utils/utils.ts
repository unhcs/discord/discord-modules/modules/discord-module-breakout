import { GuildChannel, MessageEmbed, NonThreadGuildBasedChannel, TextChannel } from "discord.js";

export function descriptionEmbed(content: string) {
    return new MessageEmbed().setDescription(content);
}

export function sendDescriptionEmbed(channel: TextChannel, content: string) {
    channel.send({ embeds: [descriptionEmbed(content)] }).catch(console.error);
    //catchPromise(channel.send, {embeds: [descriptionEmbed(content)]});
}

export function timeout(channel: TextChannel) {
    sendDescriptionEmbed(
        channel,
        `Timeout occurred waiting for user input while carrying out command.
    Please re-execute the command if you wish to try again.`
    );
}

/*export function getInteractionData(s: string, options: readonly CommandInteractionOption[] | undefined): CommandInteractionOption | undefined {
    if(!options) return undefined;

    for (let i = 0; i < options.length; i++) {
        if (options[i].name === s)
            return options[i];
    }

    return undefined;
}*/

export async function addMentionsInStringToRooms(
    mentionsString: string,
    rooms: GuildChannel[]
) {
    const mentions = mentionsString.match(/<@(!?|&)(\d+)>/g); //Should match mentions, doesn't check for correct number of snowflake digits

    const promise_arr:Promise<NonThreadGuildBasedChannel>[] = [];
    mentions?.forEach(async (m) => {
        m = m.slice(2, -1);
        if (m.startsWith("!") || m.startsWith("&")) {
            m = m.slice(1);
        }

        for (let i = 0; i < rooms.length; i++) {
            const promise = rooms[i].permissionOverwrites
                .create(m, { VIEW_CHANNEL: true });
            
            promise_arr.push(promise);
        }
    });

    await(Promise.all(promise_arr)).catch(console.error);

    return mentions;
}

export function validateRoomName(name: string) {
    //const namesimple = name.trim().toLowerCase().replaceAll(/\s+/g, "-");
    const hasAlphaNumeric = name.match(/[^\W_]/);
    if (!hasAlphaNumeric) {
        throw (
            "You entered an invalid room name, please re-execute the command with" +
            " a room name that has at least one alphanumeric character."
        );
    }

    if (name.match(/[^\w-]/) || name.match(/--+/) || !name.match(/[^\W_]/)) {
        //anything with non alphanumeric, underscore, or single hyphens is invalid
        throw (
            "You entered an invalid room name, please re-execute the command with" +
            " only alphanumeric characters, underscores, or single hyphens for the room name."
        );
    }
}

export function cleanRoomName(name: string) {
    name = name.trim().toLowerCase().replaceAll(/\s+/g, "-"); //replace chunks of whitespace with single dashes
    name = name.slice(0, 8);
    return name;
}

/*export async function validateAndCleanRoomName(name: string){

    name = name.trim().toLowerCase().replaceAll(/\s+/g, "-"); //replace chunks of whitespace with single dashes
    const hasAlphaNumeric = name.match(/[^\W_]/);
    if(!hasAlphaNumeric){
      return Promise.reject("You entered an invalid room name, please re-execute the command with"
        + " a room name that has at least one alphanumeric character.");
    }
    
    if(name.match(/[^\w-]/) || name.match(/--+/) || !name.match(/[^\W_]/)){ //anything with non alphanumeric, underscore, or single hyphens is invalid
      return Promise.reject("You entered an invalid room name, please re-execute the command with"
        + " only alphanumeric characters, underscores, or single hyphens for the room name.")
    }

    name = name.slice(0, 8);
    return name;
}*/

//export function catchPromise<T, K>(func: (params: T) => Promise<K>, params?: T){
//return func(params).catch(console.error);
//}
