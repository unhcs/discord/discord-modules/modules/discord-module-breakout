import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";
import { descriptionEmbed } from "utils/utils";
import { Subcommand } from "../Subcommand";
import { createRoomAndAddMentions, getSettings } from "./utils/BreakoutUtils";

export class BreakoutCreate extends Subcommand {
    constructor(){
        super("create");
    }

    buildSubcommand = function (subcommand: SlashCommandSubcommandBuilder) {
        return subcommand
            .setName("create")
            .setDescription("Create a breakout room.")
            .addStringOption((option) =>
                option
                    .setName("members")
                    .setDescription(
                        "The members or roles you wish to create the breakout with. Format as @ mentions."
                    )
                    .setRequired(true)
            )
            .addChannelOption((option) =>
                option
                    .setName("category")
                    .setDescription(
                        "Category or channel under category to place breakout."
                    )
                    .setRequired(false)
            )
            .addStringOption((option) =>
                option
                    .setName("name")
                    .setDescription("The name of the breakout room")
                    .setRequired(false)
            )
            .addStringOption((option) =>
                option
                    .setName("topic")
                    .setDescription("Topic for breakout.")
                    .setRequired(false)
            );
    }
    

    execute = async function (interaction: CommandInteraction): Promise<void> {
        if (!interaction.guild) return;
    
        let settings;
        try {
            settings = await getSettings(interaction);
        } catch (err) {
            if (typeof err !== "string") return;
            return resolveWithUsageError(interaction, err);
        }
    
        try {
            await createRoomAndAddMentions(interaction, settings);
            await interaction.reply({
                embeds: [descriptionEmbed("Breakout room created")],
                ephemeral: true
            });
        } catch (err) {
            console.log(err);
            if (!interaction.replied)
                await interaction.reply({
                    embeds: [
                        descriptionEmbed("Breakout room could not be created")
                    ],
                    ephemeral: true
                }).catch(console.error);
            else
                await interaction.followUp({
                    embeds: [
                        descriptionEmbed("Error occured during breakout room setup")
                    ],
                    ephemeral: true
                }).catch(console.error);
            console.error(err);
            return;
        }
        return;
    }
}

async function resolveWithUsageError(
    interaction: CommandInteraction,
    message: string
): Promise<void> {
    await interaction
        .reply({
            embeds: [descriptionEmbed(`Invalid usage: ${message}`)],
            ephemeral: true
        })
        .catch((err) => Promise.reject(err));
    return Promise.resolve();
}
