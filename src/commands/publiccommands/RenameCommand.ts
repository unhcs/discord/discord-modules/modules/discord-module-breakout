import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction, GuildMember } from "discord.js";
import {
    descriptionEmbed,
    validateRoomName,
    cleanRoomName,
} from "utils/utils";
import { Subcommand } from "../Subcommand";
import fetch from "node-fetch-commonjs"

export class RenameCommand extends Subcommand {
    constructor(){
        super("rename");
    }
    buildSubcommand = function (subcommand: SlashCommandSubcommandBuilder) {
        return subcommand
            .setName("rename")
            .setDescription("Change the name of the current breakout room")
            .addStringOption((option) =>
                option
                    .setName("new_name")
                    .setDescription(
                        "The name you would like to change the channel to. 8 characters max."
                    )
                    .setRequired(true)
            );
    }

    execute = async function (interaction: CommandInteraction): Promise<void> {
        let newName = interaction.options.get("new_name")?.value;

        if (!interaction.guild) {
            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [
                        descriptionEmbed("ERROR: command must be used in guild")
                    ]
                })
                .catch(console.error);
            return;
        }

        if (
            interaction.channel?.type !== "GUILD_TEXT" ||
            !interaction.channel.name.startsWith("breakout")
        ) {
            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [
                        descriptionEmbed(
                            "ERROR: command must be used in breakout room"
                        )
                    ]
                })
                .catch(console.error);
            return;
        }

        if (typeof newName !== "string") {
            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [
                        descriptionEmbed("ERROR: command received malformed data")
                    ]
                })
                .catch(console.error);
            return;
        }

        const channel = interaction.channel;

        //command useable by admins and room creator, breaks if topic paradigm is changed
        if (
            !channel.topic?.includes(
                `@${(interaction.member as GuildMember).displayName}`
            ) &&
            !(interaction.member as GuildMember).permissions.has("ADMINISTRATOR")
        ) {
            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [
                        descriptionEmbed(
                            "ERROR: you do not have permission to use this command in this room.\n" +
                                "If you believe this is a mistake, please contact an administrator"
                        )
                    ]
                })
                .catch(console.error);
            return;
        }

        try {
            validateRoomName(newName);
        } catch (err) {
            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [descriptionEmbed(err as string)]
                })
                .catch(console.error);
            return;
        }

        newName = cleanRoomName(newName);

        newName = "breakout-" + newName + channel.name.slice(-3);

        //find paired voice channel
        const pair = channel.guild.channels.cache.find(
            (ch) =>
                ch.parentId === channel.parentId &&
                ch.name === channel.name &&
                ch.type === "GUILD_VOICE"
        );

        newName = newName.replaceAll(/-+/g, "-"); //replaces multiple hyphens with single ones

        const requestOptions = {
            method: "PATCH",
            headers: {
                Authorization: `Bot ${process.env.DISCORD_APPLICATION_TOKEN}`,
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                name: newName
            })
        };

        //TODO: update to properly display error messages other than rate limiting.

        try {
            const res = await fetch(
                `https://discord.com/api/channels/${channel.id}`,
                requestOptions
            );

            if (res.status === 429) {
                throw "ERROR: Discord rate limit for changing channel settings has been reached.\n Please wait at least 10 minutes before trying again.";
            }

            if (pair) {
                const res2 = await fetch(
                    `https://discord.com/api/channels/${pair.id}`,
                    requestOptions
                );

                if (res2.status === 429) {
                    throw "ERROR: Discord rate limit for changing channel settings has been reached.\n Please wait at least 10 minutes before trying again.";
                }
            }
        } catch (err) {
            let errMessage;

            if (
                typeof err === "string" &&
                err.startsWith("ERROR: Discord rate limit")
            )
                errMessage = err;
            else errMessage = "Unknown error has occured";

            await interaction
                .reply({
                    ephemeral: true,
                    embeds: [descriptionEmbed(errMessage)]
                })
                .catch(console.error);

            return;
        }

        await interaction
            .reply({
                ephemeral: true,
                embeds: [
                    descriptionEmbed(
                        `Breakout channel name has been set to "${newName}"`
                    )
                ]
            })
            .catch(console.error);

        return;
    }
}
