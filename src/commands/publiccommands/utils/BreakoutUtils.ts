import {
    CategoryChannel,
    CommandInteraction,
    GuildChannel,
    GuildChannelCreateOptions,
    GuildMember,
    MessageEmbed,
    TextChannel
} from "discord.js";
import {
    addMentionsInStringToRooms,
    validateRoomName,
    cleanRoomName
} from "utils/utils";

type roomSettings = {
    name: string;
    textSettings: GuildChannelCreateOptions;
    voiceSettings: GuildChannelCreateOptions;
};

/**
 * Takes a breakout room creation command interaction and produces the appropriate
 * settings for room creation.
 * Rejects with an appropriate error message if valid settings cannot be produced.
 */
export async function getSettings(
    interaction: CommandInteraction
): Promise<roomSettings> {
    const members = interaction.options.get("members");
    const categoryData = interaction.options.get("category");
    const topic = interaction.options.get("topic");
    const name = interaction.options.get("name");

    //TODO: I believe these if checks are all redundant, need to verify before removing.
    if (!interaction.member)
        return Promise.reject(
            "Command can not be executed in direct messages."
        );

    if (!interaction.guild)
        return Promise.reject("Command must be used in a guild");

    if (interaction.channel?.type !== "GUILD_TEXT")
        return Promise.reject("Command can only be executed in guild channel.");

    const mem = interaction.member as GuildMember;
    const nick = mem.displayName;

    const textSettings: GuildChannelCreateOptions = {
        type: "GUILD_TEXT",
        topic: `@${nick}${
            topic && typeof topic.value === "string" ? " -- " + topic.value : ""
        }`
    };

    const voiceSettings: GuildChannelCreateOptions = {
        type: "GUILD_VOICE"
    };

    const channel = interaction.channel as GuildChannel;

    let cat: CategoryChannel | undefined | null | void;

    //determine parent category of breakout room if applicable
    if (!categoryData && channel.parent) {
        textSettings.parent = channel.parent;
        voiceSettings.parent = channel.parent;
        cat = channel.parent;
    } else if (categoryData) {
        if (categoryData.channel?.type === "GUILD_CATEGORY") {
            //category specified
            textSettings.parent = categoryData.channel as CategoryChannel;
            voiceSettings.parent = categoryData.channel as CategoryChannel;
            cat = categoryData.channel as CategoryChannel;
        } else if (
            (categoryData.channel?.type === "GUILD_TEXT" ||
                categoryData.channel?.type === "GUILD_VOICE") &&
            categoryData.channel.parentId
        ) {
            //channel specified
            textSettings.parent = categoryData.channel.parentId;
            voiceSettings.parent = categoryData.channel.parentId;
            cat = (await interaction.guild?.channels
                .fetch(categoryData.channel.parentId)
                .catch(console.error)) as
                | CategoryChannel
                | undefined
                | null
                | void;
        }
    }

    if (cat && cat.children.size > 48)
        return Promise.reject(
            "Too many rooms in this category to create breakout"
        );

    if (interaction.guild?.channels.channelCountWithoutThreads > 498) {
        return Promise.reject(
            "Too many channels in this server to create breakout"
        );
    }

    let channelName = "";

    let baseInput = name?.value;
    if (typeof baseInput !== "string") {
        const names = mem.displayName.toLowerCase().split(" ");
        baseInput = names[0].slice(0, 6);
        if (names.length > 1) baseInput += "-" + names[names.length - 1][0]; //add first initial of last name

        baseInput = baseInput.replaceAll(/[^\w-]/g, ""); //remove non alphanumeric/hyphen/underscore characters
    } else {
        try {
            //baseInput = await validateAndCleanRoomName(baseInput);
            validateRoomName(baseInput);
        } catch (err) {
            return Promise.reject(err);
        }
        baseInput = cleanRoomName(baseInput);
        /*baseInput = baseInput.trim().toLowerCase().replaceAll(/\s+/g, "-"); //replace chunks of whitespace with single dashes
    
    const hasAlphaNumeric = baseInput.match(/[^\W_]/);
    if(!hasAlphaNumeric){
      return Promise.reject("You entered an invalid room name, please re-execute the command with"
        + " a room name that has at least one alphanumeric character.");
    }
    
    if(baseInput.match(/[^\w-]/) || baseInput.match(/--+/) || !baseInput.match(/[^\W_]/)){ //anything with non alphanumeric, underscore, or single hyphens is invalid
      return Promise.reject("You entered an invalid room name, please re-execute the command with"
        + " only alphanumeric characters, underscores, or single hyphens for the room name.")
    }
    baseInput = baseInput.slice(0, 8);*/
    }

    baseInput = "breakout-" + baseInput;

    for (let i = 0; i < 50; i++) {
        //try up to 50 times to get unique room name
        if (i === 49) {
            return Promise.reject("Could not create unique room name");
        }

        let hex = ((Math.trunc(Math.random() * 256) + i) % 256).toString(16);
        if (hex.length === 1) hex = "0" + hex;

        channelName = `${baseInput}-${hex}`.replaceAll(/-+/g, "-"); //replace multiple hyphens with single ones

        if (
            interaction.guild &&
            !interaction.guild.channels.cache.find(
                (ch) => ch.name === channelName && ch.type === "GUILD_TEXT"
            )
        )
            break;
    }

    //check if user added any mentions to mention field
    if (
        typeof members?.value === "string" &&
        !members?.value.match(/<@(!?|&)(\d+)>/)
    ) {
        return Promise.reject(
            "You must add at least one user or role mention to the __members__ parameter to create a breakout."
        );
    }

    return {
        name: channelName,
        textSettings: textSettings,
        voiceSettings: voiceSettings
    };
}

export async function createRoomAndAddMentions(
    interaction: CommandInteraction,
    settings: roomSettings
) {
    if (!interaction.guild) return;

    const mem = interaction.member as GuildMember;

    const channels = await Promise.all([
        interaction.guild.channels.create(settings.name, {
            ...settings.textSettings,
            permissionOverwrites: [
                {
                    id: mem.id,
                    allow: ["VIEW_CHANNEL"]
                },
                {
                    id: interaction.guild.roles.everyone,
                    deny: ["VIEW_CHANNEL"]
                }
            ]
        }),
        interaction.guild.channels.create(settings.name, {
            ...settings.voiceSettings,
            permissionOverwrites: [
                {
                    id: mem.id,
                    allow: ["VIEW_CHANNEL"]
                },
                {
                    id: interaction.guild.roles.everyone,
                    deny: ["VIEW_CHANNEL"]
                }
            ]
        })
    ]).catch(console.error);

    if(!channels){
        throw "Error occured while creating guild channels";
    }

    const chan = channels[0];
    const vc = channels[1];

    const header = new MessageEmbed()
        .setTitle("Breakout")
        .setDescription(`Created by @${mem.displayName}`)
        .addFields(
            //{ name: 'Need help?', value: 'Use `/help`. If you need additional assistance, ask an @admin.' },
            {
                name: "Lifetime",
                value: "Breakouts are automatically removed once all members have left.",
                inline: true
            },
            {
                name: "Closure",
                value: "Unless requested, breakouts may close 24 hours after the last sent message.",
                inline: true
            },
            {
                name: "\\*\\*Privacy\\*\\*",
                value: "Use UNH email for sensitive info.\nThis Discord server is a public forum.",
                inline: true
            }
        );

    //content of initial breakout message mentioning all added users
    let creationMention = `<@${mem.id}>`;

    const members = interaction.options.get("members");
    if (members && typeof members.value === "string") {
        //add the mentioned User or Role to the breakout
        const added = await addMentionsInStringToRooms(members.value, [chan, vc]).catch(console.error);

        if (!added) {
            //TODO refactor to prevent breakout creation with no one in it.
        } else {
            //Filter out duplicate mentions
            const uniqueMentions: string[] = [`<@${mem.id}>`, `<@!${mem.id}>`]; //the message already includes the creator, do not neead to mention again
            const vals = added.filter((mention) => {
                if (!uniqueMentions.includes(mention)) {
                    uniqueMentions.push(mention); //ensure mention is not added again
                    return true;
                }
                return false;
            });

            vals.forEach((mention) => {
                creationMention += `, ${mention}`;
            });
        }
    }

    (chan as TextChannel).send({ content: creationMention, embeds: [header] });
}
