import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction, GuildChannel, TextChannel } from "discord.js";
import { addMentionsInStringToRooms, descriptionEmbed } from "utils/utils";
import { Subcommand } from "../Subcommand";

export class AddCommand extends Subcommand{
    constructor(){
        super("add");
    }

    buildSubcommand =  function (subcommand: SlashCommandSubcommandBuilder) {
        return subcommand
            .setName("add")
            .setDescription("Add new members or roles to the breakout")
            .addChannelOption((option) =>
                option
                    .setName("room")
                    .setDescription("The breakout room to add members to.")
                    .setRequired(true)
            )
            .addStringOption((option) =>
                option
                    .setName("members")
                    .setDescription(
                        "The members or roles you wish to add to the breakout. Format as @ mentions."
                    )
                    .setRequired(true)
            );
    }

    execute =  async function (interaction: CommandInteraction): Promise<void> {
        const data = interaction.options.data[0].options;
        if (!data) return;

        const room = interaction.options.get("room");
        const members = interaction.options.get("members");

        if (!room || !members) return;

        if (
            !(room.type === "CHANNEL") ||
            !room.channel?.name.startsWith("breakout")
        ) {
            await interaction.reply({
                embeds: [descriptionEmbed("You must select a breakout room")],
                ephemeral: true
            }).catch(console.error);
            return;
        }

        const breakout = interaction.guild?.channels.cache.get(room.channel.id);
        if (!breakout) return;

        const breakoutPair = breakout.guild.channels.cache.find((ch) => {
            return (
                ch.parentId === breakout.parentId &&
                ch.name === breakout.name &&
                (ch.type === "GUILD_VOICE" || ch.type === "GUILD_TEXT") &&
                ch.type !== breakout.type
            );
        });

        if (typeof members.value !== "string") return;

        if (!members?.value.match(/<@(!?|&)(\d+)>/)) {
            await interaction.reply({
                embeds: [
                    descriptionEmbed(
                        "You must add at least one user or role mention to the __members__ parameter to use this command"
                    )
                ],
                ephemeral: true
            }).catch(console.error);
            return;
        }

        const rooms: GuildChannel[] = [breakout as GuildChannel];
        if (breakoutPair) rooms.push(breakoutPair as GuildChannel); //TODO, consider doing more rigorous type checking here
        const added = await addMentionsInStringToRooms(members.value, rooms).catch(console.error);

        await interaction
            .reply({
                embeds: [descriptionEmbed("Members/roles have been added")],
                ephemeral: true
            })
            .catch(console.error);

        const textChan =
            breakout.type === "GUILD_TEXT"
                ? breakout
                : (breakoutPair as TextChannel);

        const uniques: string[] = [];
        added?.forEach((mention) => {
            let snowflake = mention.slice(2, -1);
            if (snowflake.startsWith("!") || snowflake.startsWith("&")) {
                snowflake = snowflake.slice(1);
            }
            if (!uniques.includes(mention)) {
                uniques.push(mention);
            }
        });

        let message = uniques[0];
        for (let i = 1; i < uniques.length; i++) {
            message += `, ${uniques[i]}`;
        }

        if(message.length !== 0)
            await textChan.send(message).catch(console.error);
            
        return;
    }
}
