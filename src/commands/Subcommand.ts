import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction } from "discord.js";

export class SubcommandGroup {
    public readonly name: string;
    public readonly description: string;

    constructor(name: string, description: string){
        this.name = name;
        this.description = description;
    }
}

export abstract class Subcommand{
    abstract buildSubcommand: (subcommand: SlashCommandSubcommandBuilder) => SlashCommandSubcommandBuilder;
    abstract execute: (interaction: CommandInteraction) => Promise<void>;
    public readonly name: string;
    public readonly group: SubcommandGroup | null;
    
    constructor(name: string, group?: SubcommandGroup){
        this.name = name;
        this.group = group ?? null;
    }
}
