import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CommandInteraction, GuildMember } from "discord.js";
import { admin } from "subcommandGroups";
import { descriptionEmbed } from "utils/utils";
import { Subcommand } from "../Subcommand";

export class CloseCommand extends Subcommand {
    constructor() {
        super("close", admin);
    }

    buildSubcommand = function (subcommand: SlashCommandSubcommandBuilder) {
        return subcommand
            .setName("close")
            .setDescription("Close breakout room");
    };

    execute = async function (interaction: CommandInteraction): Promise<void> {
        if (!interaction.member || interaction.channel?.type !== "GUILD_TEXT")
            return;

        const admin = (interaction.member as GuildMember).permissions.has(
            "ADMINISTRATOR"
        );

        const textChannel = interaction.channel;
        const voiceChannel = textChannel.guild.channels.cache.find(
            (ch) => ch.name === textChannel.name && ch.type === "GUILD_VOICE"
        );

        if (!admin) {
            await interaction
                .reply({
                    embeds: [
                        descriptionEmbed("Only admins can use this command")
                    ],
                    ephemeral: true
                })
                .catch(console.error);
        } else if (
            textChannel.name.startsWith("breakout") ||
            textChannel.name.startsWith("group")
        ) {
            await Promise.all([
                textChannel.delete(),
                voiceChannel?.delete()
            ]).catch(console.error);
        } else {
            await interaction
                .reply({
                    embeds: [
                        descriptionEmbed(
                            "Command can only be used in a breakout or group room"
                        )
                    ],
                    ephemeral: true
                })
                .catch(console.error);
        }

        return;
    };
}
