import { SlashCommandSubcommandBuilder } from "@discordjs/builders";
import { CategoryChannel, CommandInteraction, GuildMember } from "discord.js";
import { admin } from "subcommandGroups";
import { descriptionEmbed } from "utils/utils";
import {
    getSettings,
    createRoomAndAddMentions
} from "../publiccommands/utils/BreakoutUtils";
import { Subcommand } from "../Subcommand";

export class AdminCreateCommand extends Subcommand {
    constructor(){
        super("createmany", admin);
    }

    buildSubcommand = function (subcommand: SlashCommandSubcommandBuilder) {
        return subcommand
            .setName("createmany")
            .setDescription("Create a multiple breakout rooms.")
            .addStringOption((option) =>
                option
                    .setName("members")
                    .setDescription(
                        "The members or roles you wish to create the breakout with. Format as @ mentions."
                    )
                    .setRequired(true)
            )
            .addIntegerOption((option) =>
                option
                    .setName("quantity")
                    .setDescription("The number of breakout rooms to create")
                    .setRequired(true)
            )
            .addChannelOption((option) =>
                option
                    .setName("category")
                    .setDescription(
                        "Category or channel under category to place breakout."
                    )
                    .setRequired(false)
            )
            .addStringOption((option) =>
                option
                    .setName("name")
                    .setDescription("The name of the breakout room")
                    .setRequired(false)
            )
            .addStringOption((option) =>
                option
                    .setName("topic")
                    .setDescription("Topic for breakout.")
                    .setRequired(false)
            );
    }

    execute = async function (interaction: CommandInteraction): Promise<void> {
        const quant = interaction.options.get("quantity");

        if (!(interaction.member as GuildMember).permissions.has("ADMINISTRATOR")) {
            await interaction
                .reply({
                    embeds: [descriptionEmbed("Only admins can use this command")],
                    ephemeral: true
                })
                .catch(console.error);
            return;
        }

        try {
            await interaction.deferReply({ ephemeral: true });
        } catch (err) {
            await interaction.reply({
                embeds: [
                    descriptionEmbed("Error occured during command execution")
                ],
                ephemeral: true
            }).catch(console.error);
        }

        let settings;
        try {
            settings = await getSettings(interaction);
        } catch (err) {
            if (typeof err !== "string") return;
            
            await resolveWithUsageError(interaction, err).catch(console.error);
            return;
        }

        try {
            const parentResolvable = settings.textSettings.parent;
            let parent: CategoryChannel;

            if (!interaction.guild) return; //This should be impossible, as getSettings will reject if there is no guild. This is here for type checking

            if (typeof parentResolvable === "string")
                parent = (await interaction.guild.channels.fetch(
                    parentResolvable
                )) as CategoryChannel;
            else parent = parentResolvable as CategoryChannel;

            const count = quant?.value as number;
            if (parent && 50 - parent.children.size < 2 * count) {
                await resolveWithUsageError(
                    interaction,
                    "There is not enough space left in the category to create that many rooms"
                );
                return;
            }
            if (
                500 - interaction.guild.channels.channelCountWithoutThreads <
                2 * count
            ) {
                await resolveWithUsageError(
                    interaction,
                    "There is not enough space left in the server to create that many rooms"
                );
                return;
            }

            let name = settings.name + "-00";

            for (let i = 0; i < (quant?.value as number); i++) {
                name = `${name.slice(0, -3)}-${i.toString().padStart(2, "0")}`;
                const newSettings = {
                    name: name,
                    textSettings: settings.textSettings,
                    voiceSettings: settings.voiceSettings
                };
                await createRoomAndAddMentions(interaction, newSettings);
            }
            await interaction.editReply({
                embeds: [descriptionEmbed("Breakout rooms created")]
            });
        } catch (err) {
            console.log(err);
            if (!interaction.replied)
                await interaction.editReply({
                    embeds: [descriptionEmbed("Breakout room could not be created")]
                }).catch(console.error);
            else
                await interaction.followUp({
                    embeds: [
                        descriptionEmbed("Error occured during breakout room setup")
                    ],
                    ephemeral: true
                }).catch(console.error);
            console.error(err);
            return;
        }
        return;
    }
}

async function resolveWithUsageError(
    interaction: CommandInteraction,
    message: string
): Promise<void> {
    await interaction
        .editReply({ embeds: [descriptionEmbed(`Invalid usage: ${message}`)] })
        .catch((err) => Promise.reject(err));
    return;
}
